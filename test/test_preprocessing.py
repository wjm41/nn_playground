import pandas as pd
import pytest

from playground.preprocessing import load_and_featurize_esol

def test_load_esol():
    df_esol = pd.read_csv('/Users/williammccorkindale/ml_physics/nn_playground/playground/data/esol.csv')
    df_loaded = load_and_featurize_esol()
    assert df_esol.equals(df_loaded)
    
