from sklearn.metrics import r2_score, mean_squared_error
import matplotlib.pyplot as plt

def plot_training_curve(training_loss, test_loss):
    plt.plot(training_loss, label='Training Loss')
    plt.plot(test_loss, label='Test Loss')
    plt.xlabel("Step")
    plt.yscale("log")
    plt.legend()
    plt.ylabel("Loss")
    plt.show()
    return

def calc_metrics(y_true, y_pred, log=True):
    r2 = r2_score(y_true, y_pred)
    mse = mean_squared_error(y_true, y_pred)
    if log:
        print(f'R2: {r2:.4f}')
        print(f'MSE: {mse:.4f}')
    return r2, mse

def plot_correlation(y_true, y_pred, title:str = 'Train'):
    plt.scatter(y_true, y_pred)
    plt.plot([y_true.min(), y_true.max()], [y_true.min(), y_true.max()], 'k--')
    
    r2, mse = calc_metrics(y_true, y_pred)
    plt.title(f'{title}\nR2: {r2:.4f} MSE: {mse:.4f}')
    plt.xlabel("True") 
    plt.ylabel("Predicted")
    plt.show()
    return