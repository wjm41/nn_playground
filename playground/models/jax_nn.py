from typing import List

from tqdm import tqdm
import numpy as np

from jax import random, grad
import jax
import jax.numpy as jnp
from jax.nn import leaky_relu

def random_layer_params(m:int, n:int, key, scale=1e-2):
  w_key, b_key = random.split(key)
  return scale * random.normal(w_key, (n, m)), jnp.zeros(n,)

# Initialize all layers for a fully-connected neural network with sizes "sizes"
def init_network_params(sizes: List[int], key = random.PRNGKey(0)):
  keys = random.split(key, len(sizes))
  return [random_layer_params(m, n, k) for m, n, k in zip(sizes[:-1], sizes[1:], keys)]

def predict(params, fp):
  # per-example predictions
  activations = fp
  for w, b in params[:-1]:
    outputs = jnp.dot(w, activations) + b
    activations = leaky_relu(outputs)
  
  w, b = params[-1]
  outputs = jnp.dot(w, activations) + b
  return outputs

def eval_model(params, fp):
    return jnp.reshape(jax.vmap(predict, in_axes=(None, 0))(params, fp), (-1,1))


def mse_loss(y_true, y_pred):
    return jnp.mean((y_true - y_pred) ** 2)

def loss_wrapper(params, x, y_true):
    y_pred = eval_model(params, x).reshape(-1,1)
    loss_val = mse_loss(y_true, y_pred)

    return loss_val

@jax.jit
def update(params, x, y, step_size):
  grads = grad(loss_wrapper)(params, x, y)
  
  new_weights_and_biases = []
  for (w, b), (dw, db) in zip(params, grads):
      new_weights_and_biases.append((w - step_size * dw, b - step_size * db))
  return new_weights_and_biases

def train_model(dataset, layer_sizes:List[int], steps:int = 100, batch_size:int = 32, lr:float = 1e-3):
    train_x, train_y = dataset.train_x, dataset.train_y.reshape(-1,1)
    test_x, test_y = dataset.test_x, dataset.test_y.reshape(-1,1)
    
    model_params = init_network_params(layer_sizes)

    training_loss_vals = []
    test_loss_vals = []
    
    for batch in tqdm(range(steps), desc='Batch'):
        batch_inds = np.random.choice(len(train_x), size=batch_size, replace=False)
        batch_x = train_x[batch_inds]
        batch_y = train_y[batch_inds]

        train_loss = mse_loss(batch_y, eval_model(model_params, batch_x))
        test_loss = mse_loss(test_y, eval_model(model_params, test_x))
        model_params = update(model_params, batch_x, batch_y, lr)
        
        training_loss_vals.append(train_loss)
        test_loss_vals.append(test_loss)

    return training_loss_vals, test_loss_vals, model_params