from tqdm import tqdm
import numpy as np
import jax
import jax.numpy as jnp

@jax.jit
def mse_loss(y_true, y_pred):
    return jnp.mean((y_true - y_pred) ** 2)

def init_params(x):
    W = np.random.normal(size=x.shape[1])
    b = 0.0
    return W, b

def loss_wrapper(W, b, x, y_true):
    y_pred = eval_model(W, b, x)
    loss_val = mse_loss(y_true, y_pred)
    return loss_val


@jax.jit
def eval_model(W, b, x):
    return jnp.dot(x, W) + b

def train_model(dataset, steps:int = 100, batch_size:int = 32, lr:float = 1e-3):
    train_x, train_y = dataset.train_x, dataset.train_y
    test_x, test_y = dataset.test_x, dataset.test_y
    
    W, b = init_params(train_x)
    loss = jax.value_and_grad(loss_wrapper, (0, 1))

    training_loss_vals = []
    test_loss_vals = []
    

    for batch in tqdm(range(steps), desc='Batch'):
        batch_inds = np.random.choice(len(train_x), size=batch_size, replace=False)
        batch_x = train_x[batch_inds]
        batch_y = train_y[batch_inds]

        train_loss, grad_tensor = loss(W, b, batch_x, batch_y)
        test_loss, _ = loss(W, b, test_x, test_y)
        print(grad_tensor[0])
        W -= lr * grad_tensor[0]
        b -= lr * grad_tensor[1]
        training_loss_vals.append(train_loss)
        test_loss_vals.append(test_loss)

    params = (W, b)
    return training_loss_vals, test_loss_vals, params