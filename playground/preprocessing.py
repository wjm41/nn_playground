from dataclasses import dataclass
import pkg_resources

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from useful_rdkit_utils import add_molecule_and_errors, mol2numpy_fp


def load_esol() -> pd.DataFrame:
    esol_stream = pkg_resources.resource_stream(__name__, 'data/esol.csv')
    df_esol = pd.read_csv(esol_stream)
    return df_esol

def featurize_df_smiles(df: pd.DataFrame, smiles_col:str = 'SMILES'):
    df_with_features = df.copy()
    add_molecule_and_errors(df_with_features, smiles_col=smiles_col, mol_col_name='mol')
    df_with_features['morgan_fp'] = df_with_features['mol'].apply(mol2numpy_fp)
    return df_with_features

def load_and_featurize_esol(subset:bool = True, seed:int =42) -> pd.DataFrame:
    if subset:
        df_esol = load_esol().sample(n=100, random_state=seed)
    else:
        df_esol = load_esol()
    df_esol_with_features = featurize_df_smiles(df_esol)
    df_esol_with_features = df_esol_with_features.rename(columns={'morgan_fp': 'x',
                                                                  'solubility': 'y'})
    return df_esol_with_features

@dataclass()
class data_morgan():
    
    def __init__(self, df: pd.DataFrame = None, 
                 x_col:str = 'x', 
                 y_col:str = 'y', 
                 test_size:float = 0.1,
                 seed:int = 42):
        self.df = df
        self.x_col = x_col
        self.y_col = y_col
        
        train_x, test_x, train_y, test_y = train_test_split(df[x_col], df[y_col], 
                                                            test_size=test_size, random_state=seed)
        
        self.train_x = np.vstack([x for x in train_x.to_numpy()])
        self.train_y = train_y.to_numpy()
        
        self.test_x =  np.vstack([x for x in test_x.to_numpy()])
        self.test_y = test_y.to_numpy()