from setuptools import find_packages, setup

setup(
    name="playground",
    version="0.1.0",
    description="A personal playground for trying out deep learning with JAX.",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['data/*.csv']},
    author="W. McCorkindale",
    license="MIT License"
)
